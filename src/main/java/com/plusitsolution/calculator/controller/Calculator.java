package com.plusitsolution.calculator.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Calculator {
	private List<String> history = new ArrayList<String>();
	
	@RequestMapping(path = "/plus", method = RequestMethod.POST)
	public float plus(@RequestParam("plusNo.1") float num1,@RequestParam("plusNo.2")float num2) {
		float total = num1+num2;
		String forHistory = num1 + " + " + num1 + " = " + total;
		history.add(forHistory);
		return total;
	}

	@RequestMapping(path = "/minus", method = RequestMethod.POST)
	public float minus(@RequestParam("minusNo.1")float num1, @RequestParam("minusNo.2")float num2) {
		float total = num1-num2;
		String forHistory = num1 + " - " + num2 + " = " + total;
		history.add(forHistory);
		return total;
	}
	
	@RequestMapping(path = "/multiple", method = RequestMethod.POST)
	public float multiple(@RequestParam("multipleNo.1")float num1, @RequestParam("multipleNo.2")float num2) {
		float total =  num1*num2;
		String forHistory = num1 + " * " + num2 + " = " + total;
		history.add(forHistory);
		return total;
	}
	
	@RequestMapping(path = "/divide", method = RequestMethod.POST)
	public float divide(@RequestParam("divideNo.1")float num1, @RequestParam("divideNo.2")float num2) {
		float total =  num1/num2;
		String forHistory = num1 + " / " + num2 + " = " + total;
		history.add(forHistory);
		return total;
	}
	
	
	@RequestMapping(path = "/history", method = RequestMethod.GET)
	public List<String> history() {
		return history;
	}
	
	@RequestMapping(path = "/positionInHistory", method = RequestMethod.GET)
	public String positionInHistory(@RequestParam("position")int position) {
		return history.get(position);
	}
}
